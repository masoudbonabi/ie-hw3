
var game_container;
var game_list;

function getXml(url, fallback) {
    var xhttp
    xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if(this.readyState==4 && this.status==200 && typeof fallback === 'function'){
            fallback(this.responseXML);
        }
    };
    xhttp.open("GET", url, true);
    xhttp.send(null);
}

window.onload=function (){
    getXml("https://pi0.ir/ie/home.xml/", function(data) {
        home_process(data);
    });
}

function gameInfo(game) {
   var inf={
        'active':   game.getAttribute('active'),
        'hover':    game.getElementsByTagName('text')[0].getAttribute('hover'),
        'color':    game.getElementsByTagName('text')[0].getAttribute('color')
    };

    var fields = ['name', 'image', 'url', 'onlines', 'text'];
    fields.forEach(function(field) {
        inf[field] = game.getElementsByTagName(field)[0].innerHTML;
    });
    
    return inf;
}

function home_process(rxml) {

    var header= document.getElementsByTagName('header')[0];
    var color=rxml.getElementsByTagName('background')[0].innerHTML;
    header.style.backgroundColor=color;
    
    var pwd_icon = document.getElementById('pwd');
    var pwd_color = rxml.getElementsByTagName('pwd')[0].innerHTML;
    pwd_icon.style.color = pwd_color;
    
    var h_icon = document.getElementById('home-icon');
    h_icon.style.display = "none";
    
    var icon_color = document.getElementById('games').firstElementChild;
    var xml_icon_color = rxml.getElementsByTagName('gameicon')[0].getAttribute('color');
    icon_color.style.color = xml_icon_color;
    //var hover_icon_color = rxml.getElementsByTagName('gameicon')[0].getAttribute('hover');
   // icon_color.style.color = hover_icon_color;
    game_container = document.getElementById('main-container');
  
    
    var game_list = rxml.getElementsByTagName('games')[0].children;
    for(i = 0; i <game_list.length; i++){
        create_game(game_list[i]);
    }
    
}




function create_game(g_list){
    var gameData = gameInfo(g_list);
    game_div = document.createElement('div');
    game_div.setAttribute('class','game-block');
    game_div.setAttribute('id', gameData.name +'-block');
    game_div.setAttribute('data-onlines', gameData.onlines);
    
    var game_div_child = document.createElement('div');
    game_div_child.setAttribute('class','game-image-container');
    
    var game_img = document.createElement('img');
    game_img.setAttribute('src', gameData.image);
    
    var game_p = document.createElement('p');
    game_p.innerHTML = gameData.text;
   
    game_container.appendChild(game_div);
    game_div.appendChild(game_div_child);
    game_div.appendChild(game_p);
    game_div_child.appendChild(game_img);
    
    if(gameData.name == 'sudoku') {
        // bind event to game div
        game_div.onclick = function() {
            getXml('https://pi0.ir/ie/sudoku.xml/', function(data) {
                game_container.innerHTML = document.getElementById('sudoku-template').innerHTML; 
                
                var sudokuXml = data.getElementsByTagName('sudoku')[0];
                var selectedNumberColor = sudokuXml.getAttribute('selectedNumberColor');
                var selectedNumberBg = sudokuXml.getAttribute('selectedNumberBackColor');
                var hover = sudokuXml.getAttribute('hover');

                for(var rowIndex = 0; rowIndex < data.getElementsByTagName('row').length; rowIndex++) {
                    var row = data.getElementsByTagName('row')[rowIndex];

                    for(var cellIndex = 0; cellIndex < row.getElementsByTagName('cell').length; cellIndex++) {
                        var cell = row.getElementsByTagName('cell')[cellIndex];
                        
                        var $cell = $('#sudoku').find('tr').eq(rowIndex).find('td').eq(cellIndex);
                        $cell.html(cell.innerHTML);
                        $cell.attr('contenteditable', cell.innerHTML == '' ? 'true' : 'false');
                        $cell.attr('data-number', cell.innerHTML);
                    }
                }
                var $previousElems;
                $(document).on('click', '#sudoku td', function(evt) {
                    if($previousElems) {
                        $previousElems.css('background-color', '');
                        $previousElems.css('color', '');
                    }
                    var element = $(evt.target);
                    var selectedNumber = element.attr('data-number');
                    if(selectedNumber == '') {
                        return;
                    }
                    var $elems = $('[data-number="' + selectedNumber + '"]');
                    $elems.css('background-color', selectedNumberBg);
                    $elems.css('color', selectedNumberColor);
                    $previousElems = $elems;

                });
                $(document).on('keyup', 'td', function(evt) {
                    var $elem = $(this);
                    if(evt.which < 49 || evt.which > 57){
                        $elem.html('');
                    } else {
                        $elem.attr('data-number', evt.which - 48);
                        $elem.html(evt.which - 48);
                        $elem.blur();
                    }
                });
                $('#check-sudoku').click(function() {
                    for(var i = 0; i < 9; i++) {
                        var hasError = false;
                        var rowNums = [];
                        var row = $('#sudoku').find('tr').eq(i);
                        for(var j = 0; j < 9; j++) {
                            number = row.find('td').eq(j).attr('data-number');
                            if(rowNums[number] == true){
                                hasError = true;
                            }
                            rowNums[number] = true;
                        }
                        if(hasError){
                            row.css('background-color', 'red');
                        }
                    }
                });
            });
            game_container.innerHTML = 'Loading. Please Wait...';
        };
        game_div.onclick();
    }
}
